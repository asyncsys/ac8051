//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////Asyncsys Lab////////////////////////////////////////
// Author: Zecheng Liang,Zhenbang Kang
// Email: kangzhb20@lzu.edu.cn
// Create Date: 2022/06/10 
// Module Name: chip_top
// Project Name: Async_8051
// Target Devices: 
// Tool Versions: keil5
// Description: uart_project 
// Revision: V1.0
//////////////////////////////////////////////////////////////////////////////////
#include<REG320.H>
#define uchar unsigned char
uchar i;
uchar code table[]="====UART TEST====\r\nHello World!\r\nNow Send:\r\n";

void uart_putc(uchar c)
{        
    UART_TXDATA = c; // WAIT TX IDLE
} 


uchar uart_getc()
{
    return UART_RXDATA;
}

void main()
{

	for(i=0;i<sizeof(table);i++) 
	{
		uart_putc(table[i]);			//hello  (Example. tx)		
	}

	while(1)
	{
		uart_putc(uart_getc());	  //Repeat  (Example. rx & tx)
	}
}
# keil工程配置

#### step1 选择芯片型号

选择芯片型号为Synopsys的DW-8051，keil5中支持该芯片的头文件扩展包<REG320.H>。若找不到该芯片则需读者自行在keil5的官网上免费下载。工程所有的设置均为默认设置，无需修改。

<div align=center> <img src="../pic/image-20220614143835473.png" alt="chip_choose" style="zoom: 10%;" /> </div>

#### step2 添加和补充程序

C语言程序为uart_test.c，将该程序加载进该工程即可。

<div align=center> <img src="../pic/image-20220614144627702.png" alt="c_program" style="zoom: 10%;" /> </div>

- 由于UART是我们自己设计的，原扩展包内没有，因此需要在<REG320.H>中手动添加一下UART的RX寄存器和TX寄存器的地址。

<div align=center> <img src="../pic/image-20220614144836844.png" alt="c_extension" style="zoom: 10%;" /> </div>

#### step3 编译程序

点击下面红色小框中按钮编译程序，若程序是正确的，则红色大框中会出现“0 Error(s), 0 Warning(s).”字样，则程序可以使用。

<div align=center> <img src="../pic/image-20220614145028718.png" alt="compile" style="zoom: 10%;" /> </div>

- 然后点击下图中红色小框进入程序编译模式，会显示机器码，绿色框框中就是C程序对应的机器码。
<div align=center> <img src="../pic/image-20220614145245550.png" alt="code" style="zoom: 10%;" /> </div>

#### step4 生成coe文件			

然后将所有的机器码粘贴到一个txt文本中，将文本后缀改名为"xx.coe"，并用notepad软件打开。删除所有地址和注释，只留下机器码，下图中红色框代表机器码地址，绿色框代表程序注释。

<div align=center> <img src="../pic/image-20220614145842130.png" alt="coe" style="zoom: 10%;" /> </div>

- 紧接着为coe文件添加文件描述信息，即可得到可以使用的“uart_test.coe"文件。然后将该文件导入Icache对应的bram即可。

<div align=center> <img src="../pic/image-20220614153033597.png" alt="bram" style="zoom: 10%;" /> </div>




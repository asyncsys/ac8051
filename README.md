# ac8051简介（Introduction）2022.06.10

### 架构图		

Synopsys公司开发的同步DW8051是一个由状态机堆叠机制实现的两级流水线MCU，目前它已开源，在Github和CSDN上都可以下载到相关文件。本款全异步8051MCU是由DW8051转化而来的，我们的异步设计在不改变它的MCU核内电路逻辑结构的基础上，实现了同异步转换，其架构图如下图所示。

<div align=center> <img src="pic/async_cpu.png" alt="async_cpu" style="zoom: 10%;" /> </div>

### 项目内各个文件夹内容情况说明：

```
fpga------FPGA版本的异步MCU。

       |---bitstream：可以直接使用的比特流文件，用Vivado直接加载即可。

       |---coe:MCU的执行程序，将该文件加载在Icache对应的bram IP内即可直接使用.

       |---doc:模块说明和一些注意事项。

       |---rtl:MCU的电路代码。

       |---xdc:MCU的约束文件。

keil5-----生成coe所用的keil工程

tb--------MCU仿真用的测试文件
```

## 快速上手（Quick Start）

### 硬件资源：

准备一块带可编程UART的FPGA开发板，本项目选择的是ALINX公司的A7系列开发板（注意：ALINX公司常用的的Zynq系列开发板不带可编程UART），芯片型号为XC7A100TFGG484-2。开发板实物图如下图所示，可以在淘宝ALINX官方旗舰店买到该开发板。

<div align=center> <img src="pic/FPGA.png" alt="FPGA" style="zoom:100%;" /> </div>

### 软件环境：

keil5和FPGA对应的开发软件（如用Xilinx系列的FPGA，则用Vivado即可），本项目的工程环境选择的是Vivado2019.1。

### 工程配置：

**Step1:** 把"./fpga/rtl"文件夹内所有的“xx.v”文件添加到vivado工程中，添加好后的工程情况如下图所示：

<div align=center> <img src="pic/project1.png" alt="project1" style="zoom: 20%;" /> </div>

**Step2:** 修改“chip_top.v”,“mcu.v”,“DW8051_core.v”,“DW8051_biu.v”和“uart_unit.v”中的头文件路径，如下图所示。注意这个地方的头文件路径一定要写绝对路径，写相对路径会导致综合时掉模块且不报错。图4中红框里的部分写你自己电脑里存放这两个.v文件的位置。

<div align=center> <img src="pic/include_modify.png" alt="include_modify" style="zoom: 20%;" /> </div>

- 修改完成后的工程情况如下图所示，由该图可知，在**Step1**中的Non-module Files部分在添加绝对路径后被Vivado自动修改掉了，至此**Step2**完成。若在Design Sources中除了Verilog文件夹外还出现了其他文件夹，则说明头文件路径未全部修改完成，需要寻找缺失部分并添加修改。

<div align=center> <img src="pic/project2.png" alt="project2" style="zoom: 67%;" /> </div>

**Step3:** 添加bram IP核。Icache和Dcache均采用bram IP，Icache的配置情况如下图所示。Write Width修改为8，Write Depth修改为8192，即Icache配置的存储容量为8KB。Operating Mode选择Read First，取消勾选Primitives Output Register。

<div align=center> <img src="pic/Icache.png" alt="Icache" style="zoom: 50%;" /> </div>

- Dcache的配置情况如下图所示，Write Width修改为8，Write Depth修改为256，即Dcache配置的存储容量为256B。Operating Mode选择Read First，取消勾选Primitives Output Register。

<div align=center> <img src="pic/Dcache.png" alt="Dcache" style="zoom: 50%;" /> </div>

- DW8051支持0-16KB范围的Icache，0-256B范围的Dcache和0-16KB的外部cache。但是外部cache不是必须的，在keil编译器中只有程序大于2KB才需要使用外部cache，通常情况下不会用8051做太大的程序处理，因此本项目只做了Icache和Dcache。（注意：先添加Icache后添加Dcache则小问号消失，顺序有变化需要读者自行修改Icache和Dcache内模块的例化名，推荐读者按先Icache再Dcache添加bram）。配置完成后的工程情况如下图所示，此时该Vivado工程中上方小问号消失，**Step3**完成。

<div align=center> <img src="pic/image-20220610170908215.png" alt="image-20220610170908215" style="zoom: 67%;" /> </div>

**Step4:** 添加约束文件。在./fpga/xdc文件夹内提供了本开发板的约束文件。若采用的开发板不同则需要根据具体的引脚信息修改约束文件。

**Step5:** 在Icache中加载coe文件。它是我们用keil5软件编译好的测试程序，在“./fpga/coe”文件夹内。测试程序的内容为MCU复位后首先向上位机发送一串字符串。在上位机显示后MCU开始接受由上位机发送来的字符，并在接收后发送回上位机。

**Step6:** 生成比特流，并打开串口验证。串口的波特率配置为115200。先打开串口调试助手，然后将比特流文件烧录至FPGA。在烧录完成后会出现如下图所示现象，MCU向上位机发送“hello lzu”程序，此时意味着串口发数成功。

<div align=center> <img src="pic/tx.png" style="zoom: 67%;" /> </div>

- 然后在发送区随便输一个字符比如“T”，连续点击手动发送，则“T”会被连续显示在屏幕上如下图所示。出现这些现象意味着串口收数成功。

<div align=center> <img src="pic/rx.png" style="zoom: 67%;" /> </div>

## 直接使用（Directly Use）

将./fpga/bitstream文件夹内“chip_top.bit”文件直接烧录到FPGA开发板。

## 演示视频（Video）

上述步骤的演示过程如下：../pic/v1_演示视频.mp4


## 功耗分析（Power Anaylsis）

#### Normal模式（Vivado中Implementation里的Report Power选项）：

<div align=center> <img src="pic/power1.png" alt="power1" style="zoom: 67%;" /> </div>

<div align=center> <img src="pic/image-20220610223004683.png" alt="image-20220610223004683" style="zoom:67%;" /> </div>

<div align=center> <img src="pic/d4cd6221c49b4b11aba6cc60b03ed6a.png" alt="d4cd6221c49b4b11aba6cc60b03ed6a" style="zoom:67%;" /> </div>

#### SAIF模式，配置方式如下：

SAIF模式功耗分析估计的是芯片实际使用时的功耗情况，Normal模式功耗分析统计的是所有基本电路单元的功耗估计值。因此SAIF模式的功耗分析比Normal模式更为准确，在ASIC设计中使用PTPX也需要"xx.saif"文件分析实际功耗。配置方法如下：

**Step1:** 在设置中找到Simulation选项，然后在红色方框处命名一个“xx.saif文件”。

<div align=center> <img src="pic/17aa87a0c3e3f0a8175be617f6d5fc6.png" alt="17aa87a0c3e3f0a8175be617f6d5fc6" style="zoom:67%;" /> </div>

**Step2:** 进行Post-Implementation Timing Simulation仿真，即可生成“test.saif”文件。

**Step3:** 在红框内路径找到"test.saif"文件。

<div align=center> <img src="pic/image-20220610230520418.png" alt="image-20220610230520418" style="zoom: 50%;" /> </div>

- 然后将它添加到Report Power-->Switching-->Simulation activity选项中，点击OK即可得到SAIF模式的功耗分析。

<div align=center> <img src="pic/image-20220610230650734.png" alt="image-20220610230650734" style="zoom: 67%;" /> </div>

- 功耗分析结果为：

<div align=center> <img src="pic/image-20220610230911505.png" alt="image-20220610230911505" style="zoom:67%;" /> </div>

<div align=center> <img src="pic/image-20220610230943705.png" alt="image-20220610230943705" style="zoom:67%;" /> </div>

<div align=center> <img src="pic/image-20220610231014324.png" alt="image-20220610231014324" style="zoom:67%;" /> </div>

- 可以看到在实际MCU运行的过程中，MCU Core的功耗仅有1毫瓦。

## 其他（Others）

- 各模块详细说明参见./fpga/doc文件夹里的文件说明。

- MCU测试工程编写参加./fpga/keil5文件夹里的文件说明。

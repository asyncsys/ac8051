`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2022/02/17 20:12:58
// Design Name: 
// Module Name: testbench
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module testbench;
    reg             rst_n;
    reg             clk_in1_p;
    reg             uart_rx;
//    reg             in_R;
//    wire [7:0]      sfr_data_out;
    wire             clk_in1_n;
    wire             uart_tx;
    chip_top uut(
                .rst_n(rst_n),
//                .rst_cpu(in_R),
//                .sfr_data_out(sfr_data_out)
                .sys_clk_p(clk_in1_p),
                .sys_clk_n(clk_in1_n),
                .uart_rx(uart_rx),
                .uart_tx(uart_tx)
		                );
    parameter BPS_115200 = 8680;
    parameter SEND_DATA  = 8'b1010_0011;
    integer i = 0;
    always #2.5 clk_in1_p = ~ clk_in1_p;   //20ns一个周期，产生50MHz时钟源
    assign clk_in1_n=~clk_in1_p;	                
    initial begin
        rst_n       = 1'b1;
//        in_R        = 1'b1;
        clk_in1_p   = 1'b0;
        rst_n = 'b1;
        uart_rx = 1'b1;
        #2
        rst_n = 'b0;
        #2
        rst_n = 'b1;
        #26
        rst_n = 'b0;
        #30
        rst_n = 'b1;
        #450
        rst_n = 'b0;
        #540
        rst_n = 'b1;
        #2
        rst_n = 'b0;
        #23
        rst_n = 'b1;
        #536
        rst_n = 'b0;
        #1
        rst_n = 'b1;
        #20
        rst_n = 'b0;
        #100
        rst_n = 'b1;
        #500
        rst_n = 'b0;
        #100
        rst_n = 'b1;
        #20
        rst_n = 'b0;
        #5
        rst_n = 'b1;
        #300
         rst_n = 'b0;
        #50
        rst_n = 'b1;
        #10
         rst_n = 'b0;
        #400
        rst_n = 'b1;
        #50
         rst_n = 'b0;
        #80
        rst_n = 'b1;
        #20000
        rst_n = 'b1;
        #50
         rst_n = 'b0;
        #80
        rst_n = 'b1;
        #2000000
        uart_rx = 1'b0;
        for(i=0;i<8;i=i+1)
        #BPS_115200 uart_rx=SEND_DATA[i];
        #BPS_115200 uart_rx=1'b0;
        #BPS_115200 uart_rx=1'b1;
//        #1000
//        in_R = 'b0;
//        #500
//        in_R = 'b1;
    end
endmodule

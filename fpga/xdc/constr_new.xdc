############## NET - IOSTANDARD #####################
set_property CFGBVS VCCO [current_design]
set_property CONFIG_VOLTAGE 3.3 [current_design]
#############SPI Configurate Setting#################
set_property BITSTREAM.CONFIG.SPI_BUSWIDTH 4 [current_design]
set_property CONFIG_MODE SPIx4 [current_design]
set_property BITSTREAM.CONFIG.CONFIGRATE 50 [current_design]
############## clock define##########################
create_clock -period 5.000 [get_ports sys_clk_p]
set_property PACKAGE_PIN R4 [get_ports sys_clk_p]
set_property IOSTANDARD DIFF_SSTL15 [get_ports sys_clk_p]
set_property PACKAGE_PIN T4 [get_ports sys_clk_n]
set_property IOSTANDARD DIFF_SSTL15 [get_ports sys_clk_n]

##############reset key define########################
set_property PACKAGE_PIN A15 [get_ports rst_n]
set_property IOSTANDARD LVCMOS33 [get_ports rst_n]
#set_property PACKAGE_PIN A16 [get_ports rst_cpu]
#set_property IOSTANDARD LVCMOS33 [get_ports rst_cpu]
############## usb uart define########################
set_property IOSTANDARD LVCMOS33 [get_ports uart_rx]
set_property PACKAGE_PIN Y12 [get_ports uart_rx]

set_property IOSTANDARD LVCMOS33 [get_ports uart_tx]
set_property PACKAGE_PIN Y11 [get_ports uart_tx]
#############LED Setting##############################
#set_property PACKAGE_PIN C17 [get_ports led]
#set_property IOSTANDARD LVCMOS33 [get_ports led]


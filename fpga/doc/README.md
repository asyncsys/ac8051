# 模块详细情况介绍

在介绍本MCU具体模块前，先说明一下大家在异步电路开发中会遇到的一些问题和解决方法：

1. 本次设计采用的click异步控制器在fpga版本里没有复位键（rst_n），但click需要有复位功能。因此在设计异步电路时，click需要有一个不被全局复位信号影响的自复位电路模块。在chip_top.v中展示了这种方法：

<div align=center> <img src="pic/issue1.png" style="zoom:20%;" /> </div>

- 上述图片描述的电路功能为：在全局系统复位后，计数到第100个时钟周期时使in_R由高到低，产生复位效果。（计数器不一定要计够100个时钟周期才能复位click链，具体数值需要根据click链设计的长度自行确定。比如本次MCU中采用的click链，实测需要200ns才能复位成功，因此对于200MHz的全局时钟，至少需要40个时钟周期才能产生click链的复位信号。）这个in_R不仅可以作为click链的复位信号，还可以作为click链的启动信号。在click_control.v中，将它取反即可启动整个click链。

<div align=center> <img src="pic/issue2.png" style="zoom:33%;" /> </div>

- 它的复位功能体现在click单元的自应答，如click_control_first.v中所示。第四个延迟单元的click需要在它结束的时候自应答，此时若直接将out_R_delay_newfinal2接在click_delay4的out_A端口，则会导致click单元的时序紊乱（本质原因是click单元没有复位功能，若out_A在fire下降沿没到来时输入，则会导致下一次in_R进入时不再产生fire）。若用全局复位i_rst_n也会导致click单元的时序紊乱（全局复位会导致负责启动click链的in_R信号突然变为0，根据click的工作原理可知，若in_R在复位时对应的值为1，则它突然从1变为0可能会导致click链在复位时被启动。若出现问题的click链恰好驱动计数器或状态机，则会导致计数器和状态机下次不再从初始状态开始工作，从而导致电路运行错误）。而in_R信号是在全局复位完成后才会从1变为0的，因此它的复位足够稳定，可专门用于处理自应答click的复位。

<div align=center> <img src="pic/image-20220613105850736.png" alt="image-20220613105850736" style="zoom:67%;" /> </div>

- 切记，所有需要自应答的click单元都要用上述方式，否则一定会在click链上出现时序问题！

2. 加约束的方式（重点）：

   **step1:** 在所有例化的电路模块前加（ * keep_hierarchy = "yes" * ）,保证电路在综合和布局布线时所有的模块信号名称保持不变。

   **step2:** 对除了always语句中posedge和negedge引用的信号如fire、rst_n等以外的所有信号加上（ * dont_touch = "true" * ）,保证电路在综合和布局布线时所有的模块信号不会被优化掉。

   **step3:** 可适当删除部分不会被布局布线阶段优化掉的信号约束，以降低电路总功耗。

- 加约束的目的主要还是因为现如今的EDA设计工具都是针对同步电路设计的，它难以理解异步电路到底在做什么样的事情。所以说如果不加约束的话，在布局布线的时候click链很多信号会被莫名其妙的优化掉，甚至根本找不到它对应的名称，若电路出现了问题，难以排查。step1主要是保证模块间交互的信号不变，step2主要保证模块内信号不变。如果step1和step2少了其中的某一步，都难以保证信号的完整性，具体实现方法可以参考代码。

##### 注意一下必须要解决的约束导致的问题：

- 当出现下述问题时，就是对fire加了约束，将fire前的约束删除即可解决该问题。如果不解决该问题强行修改仿真中电路出现的问题，则会导致电路bug越改越多。修改电路bug前一定要先确定有没有该问题，没有该问题后再进行修改。

<div align=center> <img src="pic/issue5.png" style="zoom:67%;" /> </div>

## 复位滤波模块（rst_filter.v）

这是一个可以应用到几乎所有异步电路的复位滤波模块，推荐大家在设计异步电路时都加一个这个模块。

#### 为什么要复位滤波？

异步电路设计中一直存在一个问题，就是相位匹配问题。如图（b），若click单元没收到第三次应答（3rd ack），则即使第四次请求到来（4th req）也不会产生fire信号。在本次设计的MCU中，启动in_R（click_control中的process_req_out信号）会受全局复位影响。若在复位时它是高电平，且它前面的请求和应答工作均完成，则它在复位后从高点平变为低电平会导致产生一次fire。在这一次fire后，in_A，out_R，out_A会陆续变为0，完成复位。但是若全局复位信号在额外的fire还没使out_R、in_A和out_A完成复位就结束了，那么这一次的相位状态会保存在click链中，导致下次启动错误。为了解决这个问题，就必须要进行复位滤波。复位滤波的目的有两个：（1）滤除复位按键产生的毛刺信号，使只有一个稳定的复位信号进入MCU。（2）使复位信号保持一定的时间，不能短于click链自复位的时间。

<div align=center> <img src="pic/image-20220613153342589.png" alt="image-20220613153342589" style="zoom: 33%;" /> </div>

#### 设计方法：

**step1:** 在rst_n从低电平变为稳定的高电平后开始计数一段时间，等计数结束后产生第一级复位信号rst_n1。

**step2:** 在rst_n1从低电平变为稳定的高电平后开始计数一段时间，等计数结束后产生第二级复位信号rst_n2。

<div align=center> <img src="pic/image-20220613161723091.png" alt="step" style="zoom: 67%;" /> </div>

#### 注意：

- 采用两级复位的原因是避免第一级复位在复位过程中被新的复位信号打断。
- 第二级复位的计数器counter不能被全局复位信号rst_n复位，它只能被第一级复位信号rst_n1复位，否则第二级复位无效。
- 详细内部实现细节参见代码。

## 异步驱动逻辑部分（click_control.v）

异步驱动逻辑（Asynchronous Driving Logic, ADL）包含两部分，一部分是取指和译码异步驱动逻辑（IF&ID ADL），一部分是执行异步驱动逻辑（EX ADL）。IF&ID ADL对应的代码为click_control_first.v，EX ADL对应的代码为click_control_second.v。

### IF&ID ADL

一般流水线的第一级的异步驱动逻辑设计都分为三部分：启动部分（Event Start Part），执行部分（Event Driving Part）和结束部分（Event End Part）。

- Event Start Part：主要负责三个功能：（1）处理MCU启动请求。（2）处理MCU运行请求。（3）处理外设响应请求。

- MCU启动请求对应的是它复位后由计数器产生的第一个请求，该请求到来后MCU开始工作。它对应的是图中的Start_req信号（click_control.v中的fire_start信号），它只会产生一次，因为后续该驱动逻辑工作接受的请求必须是EX ADL返回的请求信号，不可能用无限复位的方式驱动MCU保持工作。也就是说，in_R第一次翻转是由Start_req决定的，之后所有翻转都是由EX_ack（click_control.v中的process_req_end信号）决定的，这也是它处理MCU运行请求的过程。外设响应请求对应的信号为I/O_ack1（click_control.v中的fire_rx_end信号）和Icache_ack（click_control.v中的out_R_icache信号），这两个请求保证了MCU和外设正确的**跨时钟域交互**。在MCU启动后，当I/O_ack1未到来时，MCU会停在fire_c2对应的阶段，直到它到来后才使MCU继续工作；Icache_ack未到来时，MCU会停在fire_c3对应的阶段，直到它到来后才使MCU进入到fire_c4阶段。由于MCU在外设工作时会绝对静止（没有fire信号驱动内部电路模块），直到外设结束工作后MCU才收到请求信号继续工作，因此我们的方法在解决跨时钟域问题时不需要异步FIFO的机制。

<div align=center> <img src="pic/image-20220613174003594.png" alt="IF&ID" style="zoom: 67%;" /> </div>

- Event Driving Part：主要负责产生fire信号来驱动MCU工作，由于DW8051是一个四阶段状态机MCU，因此需要四个fire信号来驱动BIU和它对应的状态机计数器工作。

- Event End Part：主要负责向下一级和Dcache发送启动请求。

### EX ADL

一般流水线的第二级及往后的异步驱动逻辑设计都分为两部分：执行部分（Event Driving Part）和结束部分（Event End Part）。

- Event Driving Part：主要负责产生fire信号来驱动MCU工作，由于DW8051是一个四阶段状态机MCU，因此需要四个fire信号来驱动Control等第二级流水电路模块和第二级流水状态计数器工作。
- Event End Part：主要负责向IF&ID ADL发送继续工作请求。

<div align=center> <img src="pic/image-20220613172555095.png" alt="image-20220613172555095" style="zoom:25%;" /> </div>

## 异步存储器设计（sram_control.v和dcache_control.v）

图（a）为异步Icache的结构图，图（b）为异步Dcache的结构图。由DW8051的工作原理可知，Icache仅工作在MCU的第三个状态机，Dcache工作在MCU的第1、2、4个状态机。因此对于Icache，仅在MCU的第3个状态机阶段为它匹配一个fire，使它仅在MCU进入第3个状态机时工作。第1、2、4状态机阶段没有任何信号可以驱动它，避免它在1、2、4阶段产生额外功耗。Dcache也是同理，仅第3个状态机阶段没有fire驱动它工作，避免了它在第3个状态机阶段产生额外功耗。

- 对于Icache，由于MCU第一次启动时第二级流水线不工作。因此为了保证流水线功能与同步MCU一致，Icache在MCU第一次工作时不取指，第二次工作才开始取指。

<div align=center> <img src="pic/image-20220613173324976.png" alt="image-20220613173324976" style="zoom: 50%;" /> </div>

## 异步外设设计（uart_unit.v）

下图为异步UART的结构图，（a）为RX模块的结构图，（b）为TX模块的结构图。设计思路也非常简单，当RX和TX模块接收到MCU发送的查询请求I/O_req1（uart_unit.v中的fire_rx信号）和I/O_req2（uart_unit.v中的fire_uart信号）时，若它们此时不需要工作，直接向MCU返回应答信号I/O_ack1（uart_unit.v中的fire_rx_end信号）和I/O_ack2（uart_unit.v中的fire_tx_end信号）；若它们此时需要工作，则等到Receive Part和Transmit Part工作结束后再发送应答信号I/O_ack1和I/O_ack2。

- 而它们是否需要工作是由MCU的地址线sfr_addr和读使能sfr_rd、写使能sfr_wr共同决定的。本MCU中，UART的RX寄存器对应的地址为0x94，TX寄存器对应的地址为0x95。sfr_rd为1且sfr_addr为0x94时，UART的RX寄存器向MCU发送的数据被视为有效数据，可以被MCU使用。其他情况下RX_Module收到的数据均为无效数据，并且不会使MCU停止在第二个状态机阶段。sfr_wr为1且sfr_addr为0x95时，MCU停止在第四个状态机阶段，并将TX寄存器的数据发送至外界。其他情况下TX_Module不会向外界发送数据。

<div align=center> <img src="pic/image-20220613175910472.png" alt="UART" style="zoom:25%;" /> </div>

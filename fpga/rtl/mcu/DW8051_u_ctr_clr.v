//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////Asyncsys Lab////////////////////////////////////////
// Author: Zhenbang Kang
// Email: kangzhb20@lzu.edu.cn
// Create Date: 2022/06/10 
// Module Name: DW8051_u_ctr_clr
// Project Name: ac8051 (Asynchronous Click 8051)
// Target Devices: XC7A100TFGG484-2
// Tool Versions: Vivado 2019.1
// Description:Cycle Module 
// Revision: V2.0
//////////////////////////////////////////////////////////////////////////////////

module DW8051_u_ctr_clr   (q,
//                           q_n,
                           fire,
                           clr_n,
                           rst_n
			   );
parameter width = 2;
 input  fire;
 input  clr_n;
 input  rst_n;
 output [(width-1):0] q;
// output [(width-1):0] q_n;

//------------------------------------------------------------------------------
wire fire;
(* dont_touch = "true" *)wire clr_n;
wire rst_n;
(* dont_touch = "true" *)reg [(width-1):0] q;
(* dont_touch = "true" *)wire [(width-1):0] q_n;


//---------------
// local signals:
//---------------
(* dont_touch = "true" *)wire [(width-1):0] next;

//------------------------------------------------------------------------------

    assign next = (clr_n == 1) ? (q + 1) : 0;

    always @(posedge fire or negedge rst_n)begin
      if (!rst_n)begin
	   q <= 0;
	   end
      else begin 
	   q <= next;
	   end
    end
 //   assign q_n = ~q;

endmodule

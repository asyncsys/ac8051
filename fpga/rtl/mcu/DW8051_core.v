`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////Asyncsys Lab////////////////////////////////////////
// Author: Zhenbang Kang
// Email: kangzhb20@lzu.edu.cn
// Create Date: 2022/06/10 
// Module Name: DW8051_core
// Project Name: ac8051 (Asynchronous Click 8051)
// Target Devices: XC7A100TFGG484-2
// Tool Versions: Vivado 2019.1
// Description:Async CPU Core, removed useless Sync Peripherals
// Revision: V2.0
//////////////////////////////////////////////////////////////////////////////////
`include "D:/fpga_8051/code/fpga/rtl/include/DW8051_package.v"
`include "D:/fpga_8051/code/fpga/rtl/include/DW8051_parameter.v"
module DW8051_core (fire_out_first,
                    fire_out_second,
                    fire_cyc,
                    fire_cycsecond, 
		            por_n,
//                    rst_in_n,
//                    rst_out_n,
                    test_mode_n,

                    stop_mode_n,
                    idle_mode_n,
    
                    sfr_addr,
                    sfr_data_out,
                    sfr_data_in,
                    sfr_wr,
                    sfr_rd,

                    mem_addr,
                    mem_data_out,
                    mem_data_in,
                    mem_wr_n,
                    mem_rd_n,
                    mem_pswr_n,
                    mem_psrd_n,
                    mem_ale,
                    mem_ea_n,

                    int0_n,		// External Interrupt 0, std
                    int1_n,		// External Interrupt 1, std
                    int2,		// External Interrupt 2, ext
                    int3_n,		// External Interrupt 3, ext
                    int4,		// External Interrupt 4, ext
                    int5_n,		// External Interrupt 5, ext

                    pfi,		// power fail interrupt, ext
                    wdti,		// watchdog timer intr, ext

                    rxd0_in,		// serial port 0 input
                    rxd0_out,		// serial port 0 output
                    txd0,		// serial port 0 output

                    rxd1_in,		// serial port 1 input
                    rxd1_out,		// serial port 1 output
                    txd1,		// serial port 1 output

                    t0,			// Timer 0 external input
                    t1,			// Timer 1 external input
                    t2,			// Timer/Counter2 ext.input
                    t2ex,		// Timer/Counter2 capt./reload
                    t0_out,		// Timer/Counter0 ext. output
                    t1_out,		// Timer/Counter1 ext. output
                    t2_out,		// Timer/Counter2 ext. output

                    port_pin_reg_n,
                    p0_mem_reg_n,
                    p0_addr_data_n,
                    p2_mem_reg_n,

		    iram_addr,
		    iram_data_out,
		    iram_data_in,
		    iram_rd_n,
		    iram_we1_n,
		    iram_we2_n,

		    irom_addr,
		    irom_data_out,
		    irom_rd_n,
		    irom_cs_n
// synopsys dc_script_begin
// set_design_license  {DesignWare-Foundation} -quiet
// synopsys dc_script_end
                            );

 input fire_out_first;
 input fire_out_second;
 input fire_cyc;
 input  fire_cycsecond;
 input por_n;
// input rst_in_n;
 input test_mode_n;
 input [7:0] sfr_data_in;
 input [7:0] mem_data_in;
 input mem_ea_n;
 input int0_n;
 input int1_n;
 input int2;
 input int3_n;
 input int4;
 input int5_n;
 input pfi;
 input wdti;
 input rxd0_in;
 input rxd1_in;
 input t0;
 input t1;
 input t2;
 input t2ex;
 input [7:0] iram_data_out;
 input [7:0] irom_data_out;
// output rst_out_n;
 output stop_mode_n;
 output idle_mode_n;
 output [7:0] sfr_addr;
 output [7:0] sfr_data_out;
 output sfr_wr;
 output sfr_rd;
 output [12:0] mem_addr;
 output [7:0]  mem_data_out;
 output mem_wr_n;
 output mem_rd_n;
 output mem_pswr_n;
 output mem_psrd_n;
 output mem_ale;
 output rxd0_out;
 output txd0;
 output rxd1_out;
 output txd1;
 output t0_out;
 output t1_out;
 output t2_out;
 output port_pin_reg_n;
 output p0_mem_reg_n;
 output p0_addr_data_n;
 output p2_mem_reg_n;
 output [7:0] iram_addr;
 output [7:0] iram_data_in;
 output iram_rd_n;
 output	iram_we1_n;
 output iram_we2_n;
 output [12:0] irom_addr;
 output irom_rd_n;
 output irom_cs_n;

//------------------------------------------------------------------------------
wire fire_out_first;
wire fire_out_second;
wire fire_cyc;
wire fire_cycsecond;
//wire clk;
wire por_n;
//(* dont_touch = "true" *)wire rst_in_n;
 (* dont_touch = "true" *)wire test_mode_n;
 (* dont_touch = "true" *)wire [7:0] sfr_data_in;
 (* dont_touch = "true" *)wire [7:0] mem_data_in;
 (* dont_touch = "true" *)wire mem_ea_n;
 (* dont_touch = "true" *)wire int0_n;
 (* dont_touch = "true" *)wire int1_n;
 (* dont_touch = "true" *)wire int2;
 (* dont_touch = "true" *)wire int3_n;
 (* dont_touch = "true" *)wire int4;
 (* dont_touch = "true" *)wire int5_n;
 (* dont_touch = "true" *)wire pfi;
 (* dont_touch = "true" *)wire wdti;
 (* dont_touch = "true" *)wire rxd0_in;
 (* dont_touch = "true" *)wire rxd1_in;
 (* dont_touch = "true" *)wire t0;
 (* dont_touch = "true" *)wire t1;
 (* dont_touch = "true" *)wire t2;
 (* dont_touch = "true" *)wire t2ex;
//(* dont_touch = "true" *)wire rst_out_n;
 (* dont_touch = "true" *)wire stop_mode_n;
 (* dont_touch = "true" *)wire idle_mode_n;
 (* dont_touch = "true" *)wire [7:0] sfr_addr;
 (* dont_touch = "true" *)wire [7:0] sfr_data_out;
 (* dont_touch = "true" *)wire sfr_wr;
 (* dont_touch = "true" *)wire sfr_rd;
 (* dont_touch = "true" *)wire [12:0] mem_addr;
 (* dont_touch = "true" *)wire [7:0]  mem_data_out;
 (* dont_touch = "true" *)wire mem_wr_n;
 (* dont_touch = "true" *)wire mem_rd_n;
 (* dont_touch = "true" *)wire mem_pswr_n;
 (* dont_touch = "true" *)wire mem_psrd_n;
 (* dont_touch = "true" *)wire mem_ale;
 (* dont_touch = "true" *)wire rxd0_out;
 (* dont_touch = "true" *)wire txd0;
 (* dont_touch = "true" *)wire rxd1_out;
 (* dont_touch = "true" *)wire txd1;
 (* dont_touch = "true" *)wire t0_out;
 (* dont_touch = "true" *)wire t1_out;
 (* dont_touch = "true" *)wire t2_out;
 (* dont_touch = "true" *)wire port_pin_reg_n;
 (* dont_touch = "true" *)wire p0_mem_reg_n;
 (* dont_touch = "true" *)wire p0_addr_data_n;
 (* dont_touch = "true" *)wire p2_mem_reg_n;


//---------------
// local signals:
//---------------
 (* dont_touch = "true" *)wire low;
 
//(* dont_touch = "true" *)wire t_rst_out_n;
 
 (* dont_touch = "true" *)wire timer2_sfr_cs;
 (* dont_touch = "true" *)wire t_timer2_sfr_cs;
 (* dont_touch = "true" *)wire [7:0] timer2_data_out;
 (* dont_touch = "true" *)wire [7:0] t_timer2_data_out;
 (* dont_touch = "true" *)wire t_t2_out;

 (* dont_touch = "true" *)wire serial0_sfr_cs;
 (* dont_touch = "true" *)wire t_serial0_sfr_cs;
 (* dont_touch = "true" *)wire [7:0] serial0_data_out;
 (* dont_touch = "true" *)wire [7:0] t_serial0_data_out;
 (* dont_touch = "true" *)wire serial1_sfr_cs;
 (* dont_touch = "true" *)wire t_serial1_sfr_cs;
 (* dont_touch = "true" *)wire [7:0] serial1_data_out;
 (* dont_touch = "true" *)wire [7:0] t_serial1_data_out;

 (* dont_touch = "true" *)wire [7:0] int_sfr_addr;
 (* dont_touch = "true" *)wire [7:0] int_sfr_data_out;
 (* dont_touch = "true" *)wire [7:0] int_sfr_data_in;
 (* dont_touch = "true" *)wire int_sfr_wr;
 (* dont_touch = "true" *)wire int_sfr_cs;

 (* dont_touch = "true" *)wire [12:0] t_mem_addr;
 (* dont_touch = "true" *)wire [7:0]  t_mem_data_out;
 (* dont_touch = "true" *)wire t_mem_psrd_n;

 (* dont_touch = "true" *)wire timer_sfr_cs;
 (* dont_touch = "true" *)wire [7:0] timer_data_out;

 (* dont_touch = "true" *)wire intr_sfr_cs;
 (* dont_touch = "true" *)wire [7:0] intr_data_out;
 (* dont_touch = "true" *)wire       intr_sfr_cs_0;
 (* dont_touch = "true" *)wire [7:0] intr_data_out_0;
 (* dont_touch = "true" *)wire       intr_sfr_cs_1;
 (* dont_touch = "true" *)wire [7:0] intr_data_out_1;

 (* dont_touch = "true" *)wire [2:0] tm;
 (* dont_touch = "true" *)wire t0m, t1m, t2m;

 (* dont_touch = "true" *)wire ena_t0;
 (* dont_touch = "true" *)wire ena_t1;
 (* dont_touch = "true" *)wire ena_t0_0;
 (* dont_touch = "true" *)wire ena_t1_0;
 (* dont_touch = "true" *)wire ena_t0_1;
 (* dont_touch = "true" *)wire ena_t1_1;

 (* dont_touch = "true" *)wire tf0_set, tf1_set;
 (* dont_touch = "true" *)wire t1_ofl;

 (* dont_touch = "true" *)wire rclk, tclk;
 (* dont_touch = "true" *)wire t_rclk, t_tclk;
 (* dont_touch = "true" *)wire t2_ofl;
 (* dont_touch = "true" *)wire t_t2_ofl;
 (* dont_touch = "true" *)wire tf2, exf2;
 (* dont_touch = "true" *)wire t_tf2, t_exf2;

 (* dont_touch = "true" *)wire smod0;
 (* dont_touch = "true" *)wire ri0, ti0;
 (* dont_touch = "true" *)wire t_ri0, t_ti0;
 (* dont_touch = "true" *)wire ri1, ti1;
 (* dont_touch = "true" *)wire t_ri1, t_ti1;
 (* dont_touch = "true" *)wire t_rxd0_out;
 (* dont_touch = "true" *)wire t_txd0;
 (* dont_touch = "true" *)wire t_rxd1_out;
 (* dont_touch = "true" *)wire t_txd1;

 (* dont_touch = "true" *)wire smod1;
 (* dont_touch = "true" *)wire smod1_0;
 (* dont_touch = "true" *)wire smod1_1;
 
//wire [1:0] cpu_cycle;
 (* dont_touch = "true" *)wire cpu_int_req;
 (* dont_touch = "true" *)wire [3:0] cpu_int_src;
 (* dont_touch = "true" *)wire cpu_int_ack;
 (* dont_touch = "true" *)wire cpu_int_clr;

 (* dont_touch = "true" *)wire       cpu_int_req_0;
 (* dont_touch = "true" *)wire [3:0] cpu_int_src_0;
 (* dont_touch = "true" *)wire       cpu_int_req_1;
 (* dont_touch = "true" *)wire [3:0] cpu_int_src_1;

 (* dont_touch = "true" *)wire [12:0] irom_addr;
 (* dont_touch = "true" *)wire irom_rd_n;
 (* dont_touch = "true" *)wire irom_cs_n;
 (* dont_touch = "true" *)wire [7:0] int_rom_data_out;
 (* dont_touch = "true" *)wire int_rom_rd_n;
 (* dont_touch = "true" *)wire int_rom_cs_n;

  (* dont_touch = "true" *)wire [7:0] iram_addr;
  (* dont_touch = "true" *)wire [7:0] iram_data_out;
  (* dont_touch = "true" *)wire [7:0] iram_data_in;
  (* dont_touch = "true" *)wire iram_rd_n;
  (* dont_touch = "true" *)wire iram_we1_n;
  (* dont_touch = "true" *)wire iram_we2_n;
  (* dont_touch = "true" *)wire [7:0]irom_data_out; 
//------------------------------------------------------------------------------

  assign low = 0;

  //------------------------------
  // Implementation of CPU kernel:
  //------------------------------
(* keep_hierarchy = "yes" *)DW8051_cpu #(`ram_256, `rom_addr_size, `extd_intr) i_cpu
             (.fire_out_first (fire_out_first) , 
              .fire_out_second(fire_out_second),
              .fire_cyc        (fire_cyc),
              .fire_cycsecond   (fire_cycsecond ),
              .por_n            (por_n),
//              .rst_in_n         (rst_in_n),
//              .rst_out_n        (t_rst_out_n),
              .test_mode_n      (test_mode_n),
//              .cycle            (cpu_cycle),
              .stop_mode_n      (stop_mode_n),
              .idle_mode_n      (idle_mode_n),
              .int_sfr_addr     (int_sfr_addr),
              .int_sfr_data_out (int_sfr_data_out),
              .int_sfr_data_in  (int_sfr_data_in),
              .int_sfr_wr       (int_sfr_wr),
              .int_sfr_cs       (int_sfr_cs),
              .ext_sfr_addr     (sfr_addr),
              .ext_sfr_data_out (sfr_data_out),
              .ext_sfr_data_in  (sfr_data_in),
              .ext_sfr_wr       (sfr_wr),
              .ext_sfr_rd       (sfr_rd),
              .mem_addr         (t_mem_addr),
              .mem_data_out     (t_mem_data_out),
              .mem_data_in      (mem_data_in),
              .mem_wr_n         (mem_wr_n),
              .mem_rd_n         (mem_rd_n),
              .mem_pswr_n       (mem_pswr_n),
              .mem_psrd_n       (t_mem_psrd_n),
              .mem_ale          (mem_ale),
              .mem_ea_n         (mem_ea_n),
              .port_pin_reg_n   (port_pin_reg_n),
              .p0_mem_reg_n     (p0_mem_reg_n),
              .p0_addr_data_n   (p0_addr_data_n),
              .p2_mem_reg_n     (p2_mem_reg_n),
	      .iram_addr	(iram_addr),
	      .iram_data_out	(iram_data_out),
	      .iram_data_in	(iram_data_in),
	      .iram_rd_n	(iram_rd_n),
	      .iram_we1_n	(iram_we1_n),
	      .iram_we2_n	(iram_we2_n),
              .smod             (smod0),
              .tm               (tm),
              .int_req          (cpu_int_req),
              .int_src          (cpu_int_src),
              .int_ack          (cpu_int_ack),
              .int_clr          (cpu_int_clr),
              .int_rom_data_in  (int_rom_data_out),
              .int_rom_rd_n     (int_rom_rd_n),
              .int_rom_cs_n     (int_rom_cs_n));

//  assign  t0m = tm[0];
//  assign  t1m = tm[1];
//  assign  t2m = tm[2];

  //------------------------
  // Internal ROM Interface:
  //------------------------
  assign irom_addr = t_mem_addr;
  assign irom_rd_n = int_rom_rd_n;
  assign irom_cs_n = int_rom_cs_n;
  assign int_rom_data_out = (`rom_addr_size == 0) ? 'b0 : irom_data_out;


  //--------------------------
  // Timer 0/1 implementation:
  //--------------------------
//  (* dont_touch = "true" *)DW8051_timer i_timer
//               (.clk            (clk),
//                .rst_n          (t_rst_out_n),
//                .sfr_addr       (int_sfr_addr),
//                .timer_sfr_cs   (timer_sfr_cs),
//                .timer_data_out (timer_data_out),
//                .timer_data_in  (int_sfr_data_out),
//                .sfr_wr         (int_sfr_wr),
//                .t0             (t0),
//                .t1             (t1),
//                .int0_n         (int0_n),
//                .int1_n         (int1_n),
//                .cycle          (cpu_cycle),
//                .t0m            (t0m),
//                .t1m            (t1m),
//                .ena_t0         (ena_t0),
//                .ena_t1         (ena_t1),
//                .tf0_set        (tf0_set),
//                .tf1_set        (tf1_set),
//                .t1_ofl         (t1_ofl),
//                .t0_out         (t0_out),
//                .t1_out         (t1_out));


//  //------------------------------------
//  // Implementation of interrupt control
//  // unit (normal: extd_intr=0,
//  // extended: extd_intr=1).
//  //------------------------------------

//// App Builder controls for conditional instantiation.
//// reuse-pragma startSub SmallInterruptUnit [parameter_conditional_text %subText {@extd_intr == 0}]
//// reusepragma attr GenerateIf[0] {@extd_intr == 0}
//  (* dont_touch = "true" *)DW8051_intr_0 i_intr0
//                (.clk           (clk),
//                 .rst_n         (t_rst_out_n),
//                 .sfr_addr      (int_sfr_addr),
//                 .intr_sfr_cs   (intr_sfr_cs_0),
//                 .intr_data_out (intr_data_out_0),
//                 .intr_data_in  (int_sfr_data_out),
//                 .sfr_wr        (int_sfr_wr),
//                 .cycle         (cpu_cycle),
//                 .int_req       (cpu_int_req_0),
//                 .int_src       (cpu_int_src_0[2:0]),
//                 .int_ack       (cpu_int_ack),
//                 .int_clr       (cpu_int_clr),
//                 .int0_n        (int0_n),
//                 .int1_n        (int1_n),
//                 .tf0_set       (tf0_set),
//                 .tf1_set       (tf1_set),
//                 .ri0           (ri0),
//                 .ti0           (ti0),
//                 .tf2           (tf2),
//                 .exf2          (exf2),
//                 .ena_t0        (ena_t0_0),
//                 .ena_t1        (ena_t1_0),
//                 .smod1         (smod1_0));
//// reuse-pragma endSub SmallInterruptUnit

  assign cpu_int_src_0[3] = 0;


//// App Builder controls for conditional instantiation.
//// reuse-pragma startSub BigInterruptUnit [parameter_conditional_text %subText {@extd_intr == 1}]
//// reusepragma attr GenerateIf[0] {@extd_intr == 1}
// (* dont_touch = "true" *)DW8051_intr_1 i_intr1
//                (.clk           (clk),
//                 .rst_n         (t_rst_out_n),
//                 .sfr_addr      (int_sfr_addr),
//                 .intr_sfr_cs   (intr_sfr_cs_1),
//                 .intr_data_out (intr_data_out_1),
//                 .intr_data_in  (int_sfr_data_out),
//                 .sfr_wr        (int_sfr_wr),
//                 .cycle         (cpu_cycle),
//                 .int_req       (cpu_int_req_1),
//                 .int_src       (cpu_int_src_1),
//                 .int_ack       (cpu_int_ack),
//                 .int_clr       (cpu_int_clr),
//                 .int0_n        (int0_n),
//                 .int1_n        (int1_n),
//                 .int2          (int2),
//                 .int3_n        (int3_n),
//                 .int4          (int4),
//                 .int5_n        (int5_n),
//                 .tf0_set       (tf0_set),
//                 .tf1_set       (tf1_set),
//                 .ri0           (ri0),
//                 .ti0           (ti0),
//                 .ri1           (ri1),
//                 .ti1           (ti1),
//                 .tf2           (tf2),
//                 .exf2          (exf2),
//                 .pfi           (pfi),
//                 .wdti          (wdti),
//                 .ena_t0        (ena_t0_1),
//                 .ena_t1        (ena_t1_1),
//                 .smod1         (smod1_1));
//// reuse-pragma endSub BigInterruptUnit

//  assign intr_sfr_cs   = (`extd_intr == 1) ? intr_sfr_cs_1   : intr_sfr_cs_0;
//  assign intr_data_out = (`extd_intr == 1) ? intr_data_out_1 : intr_data_out_0;
//  assign cpu_int_req   = (`extd_intr == 1) ? cpu_int_req_1   : cpu_int_req_0;
//  assign cpu_int_src   = (`extd_intr == 1) ? cpu_int_src_1   : cpu_int_src_0;
//  assign ena_t0        = (`extd_intr == 1) ? ena_t0_1        : ena_t0_0;
//  assign ena_t1        = (`extd_intr == 1) ? ena_t1_1        : ena_t1_0;
//  assign smod1         = (`extd_intr == 1) ? smod1_1         : smod1_0;


//  //--------------------------
//  // Implementation of Timer 2
//  // (if desired)
//  //--------------------------

//// App Builder controls for conditional instantiation.
//// reuse-pragma startSub Timer2_Instance [parameter_conditional_text %subText {@timer2 == 1}]
//// reusepragma attr GenerateIf[0] {@timer2 == 1}
//  (* dont_touch = "true" *)DW8051_timer2 i_timer2
//                (.clk             (clk),
//                 .rst_n           (t_rst_out_n),
//                 .sfr_addr        (int_sfr_addr),
//                 .timer2_sfr_cs   (t_timer2_sfr_cs),
//                 .timer2_data_out (t_timer2_data_out),
//                 .timer2_data_in  (int_sfr_data_out),
//                 .sfr_wr          (int_sfr_wr),
//                 .cycle           (cpu_cycle),
//                 .t2m             (t2m),
//                 .t2              (t2),
//                 .t2ex            (t2ex),
//                 .t2_out          (t_t2_out),
//                 .out_rclk        (t_rclk),
//                 .out_tclk        (t_tclk),
//                 .out_t2_ofl      (t_t2_ofl),
//                 .out_tf2         (t_tf2),
//                 .out_exf2        (t_exf2));
//// reuse-pragma endSub Timer2_Instance

//  assign  timer2_sfr_cs    = (`timer2 == 1) ? t_timer2_sfr_cs   : 0;
//  assign  timer2_data_out  = (`timer2 == 1) ? t_timer2_data_out : 'b0;
//  assign  rclk             = (`timer2 == 1) ? t_rclk   : 0; //defaults to timer 1
//  assign  tclk             = (`timer2 == 1) ? t_tclk   : 0; //defaults to timer 1
//  assign  t2_out           = (`timer2 == 1) ? t_t2_out : 0;
//  assign  t2_ofl           = (`timer2 == 1) ? t_t2_ofl : 0;
//  assign  tf2              = (`timer2 == 1) ? t_tf2    : 0;
//  assign  exf2             = (`timer2 == 1) ? t_exf2   : 0;



//  //----------------------------------
//  // Implementation of standard serial
//  // Interface (if desired)
//  //----------------------------------
//  // Serial port 0:

//// App Builder controls for conditional instantiation.
//// reuse-pragma startSub Serial0_Instance [parameter_conditional_text %subText {@serial > 0}]
//// reusepragma attr GenerateIf[0] {@serial > 0}
//  (* dont_touch = "true" *)DW8051_serial #(`scon0_addr) i_serial1
//                (.clk             (clk),
//                 .rst_n           (t_rst_out_n),
//                 .sfr_addr        (int_sfr_addr),
//                 .serial_sfr_cs   (t_serial0_sfr_cs),
//                 .serial_data_out (t_serial0_data_out),
//                 .serial_data_in  (int_sfr_data_out),
//                 .sfr_wr          (int_sfr_wr),
//                 .t1_ofl          (t1_ofl),
//                 .rclk            (rclk),
//                 .tclk            (tclk),
//                 .t2_ofl          (t2_ofl),
//                 .cycle           (cpu_cycle),
//                 .smod            (smod0),
//                 .ri              (t_ri0),
//                 .ti              (t_ti0),
//                 .rxd_out         (t_rxd0_out),
//                 .rxd_in          (rxd0_in),
//                 .txd             (t_txd0));
//// reuse-pragma endSub Serial0_Instance

//  assign  serial0_sfr_cs   = (`serial > 0) ? t_serial0_sfr_cs   : 0;
//  assign  serial0_data_out = (`serial > 0) ? t_serial0_data_out : 'b0;
//  assign  ri0              = (`serial > 0) ? t_ri0      : 0;
//  assign  ti0              = (`serial > 0) ? t_ti0      : 0;
//  assign  rxd0_out         = (`serial > 0) ? t_rxd0_out : 1;
//  assign  txd0             = (`serial > 0) ? t_txd0     : 1;

//  // Serial port 1:
//// App Builder controls for conditional instantiation.
//// reuse-pragma startSub Serial1_Instance [parameter_conditional_text %subText {@serial == 2}]
//// reusepragma attr GenerateIf[0] {@serial == 2}
//  (* dont_touch = "true" *)DW8051_serial	#(`scon1_addr) i_serial2
//                (.clk             (clk),
//                 .rst_n           (t_rst_out_n),
//                 .sfr_addr        (int_sfr_addr),
//                 .serial_sfr_cs   (t_serial1_sfr_cs),
//                 .serial_data_out (t_serial1_data_out),
//                 .serial_data_in  (int_sfr_data_out),
//                 .sfr_wr          (int_sfr_wr),
//                 .t1_ofl          (t1_ofl),
//                 .rclk            (low),
//                 .tclk            (low),
//                 .t2_ofl          (low),
//                 .cycle           (cpu_cycle),
//                 .smod            (smod1),
//                 .ri              (t_ri1),
//                 .ti              (t_ti1),
//                 .rxd_out         (t_rxd1_out),
//                 .rxd_in          (rxd1_in),
//                 .txd             (t_txd1));
//// reuse-pragma endSub Serial1_Instance

//  assign  serial1_sfr_cs   = (`serial == 2) ? t_serial1_sfr_cs   : 0;
//  assign  serial1_data_out = (`serial == 2) ? t_serial1_data_out : 'b0;
//  assign  ri1              = (`serial == 2) ? t_ri1      : 0;
//  assign  ti1              = (`serial == 2) ? t_ti1      : 0;
//  assign  rxd1_out         = (`serial == 2) ? t_rxd1_out : 1;
//  assign  txd1             = (`serial == 2) ? t_txd1     : 1;



//  //-------------------------
//  // sfr_data_in multiplexer:
//  //-------------------------
//  assign int_sfr_data_in = (timer_sfr_cs   == 1) ? timer_data_out   :
//                           (intr_sfr_cs    == 1) ? intr_data_out    :
//                           (serial0_sfr_cs == 1) ? serial0_data_out :
//                           (serial1_sfr_cs == 1) ? serial1_data_out :
//                                                   timer2_data_out;

//  assign  int_sfr_cs  = timer_sfr_cs   |
//                        intr_sfr_cs    |
//                        serial0_sfr_cs |
//                        serial1_sfr_cs |
//                        timer2_sfr_cs;


//  // assignments for temporary signals:
//  assign  rst_out_n    = t_rst_out_n;
  assign  mem_addr     = t_mem_addr;
  assign  mem_data_out = t_mem_data_out;
  assign  mem_psrd_n   = t_mem_psrd_n;


endmodule

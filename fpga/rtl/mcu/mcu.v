`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////Asyncsys Lab////////////////////////////////////////
// Author: Zhenbang Kang
// Email: kangzhb20@lzu.edu.cn
// Create Date: 2022/06/10 
// Module Name: mcu
// Project Name: ac8051 (Asynchronous Click 8051)
// Target Devices: XC7A100TFGG484-2
// Tool Versions: Vivado 2019.1
// Description:MCU Module 
// Revision: V2.0
//////////////////////////////////////////////////////////////////////////////////
`include "D:/fpga_8051/code/fpga/rtl/include/DW8051_package.v"
`include "D:/fpga_8051/code/fpga/rtl/include/DW8051_parameter.v"

module mcu(
           fire_out_first,
           fire_out_second,
           fire_cyc,
           fire_cycsecond,
           por_n        , 
//           rst_in_n     , 
//           rst_out_n    , 
           test_mode_n  , 
             
           stop_mode_n  , 
           idle_mode_n  ,         
           
           int0_n       , 
           int1_n       , 
           int2         , 
           int3_n       , 
           int4         , 
           int5_n       , 
           
           pfi          ,  //power failed
           wdti         ,  //watchdog
           
           rxd0_in      , 
           rxd0_out     , 
           txd0         , 
           
           rxd1_in      , 
           rxd1_out     , 
           txd1         , 
           
           t0           , 
           t1           , 
           t2           , 
           t2ex         , 
           t0_out       , 
           t1_out       , 
           t2_out       , 
           
           iram_addr    , 
           iram_data_out,  //mcu data to iram 
           iram_data_in ,  //iram data to mcu
           iram_we_n    ,
           
           irom_addr    ,  
           irom_data    , 
           irom_rd_n    ,

           sfr_addr     , 
           sfr_data_out , 
           sfr_data_in  , 
           sfr_wr       , 
           sfr_rd       ,
           
           mem_addr     , 
           mem_data_out , 
           mem_data_in  , 
           mem_wr_n     , 
           mem_rd_n 
                                                       );
													   
           input                         fire_out_first;
           input                         fire_out_second;
           input                         fire_cyc;
           input                         fire_cycsecond;
           input                         por_n        ; 
//           input                         rst_in_n     ; 
//           output                        rst_out_n    ; 
           input                         test_mode_n  ; 
                                           
           output                        stop_mode_n  ; 
           output                        idle_mode_n  ;         
                     
           //int
           input                         int0_n       ; 
           input                         int1_n       ; 
           input                         int2         ; 
           input                         int3_n       ; 
           input                         int4         ; 
           input                         int5_n       ; 
           
           input                         pfi          ;  //power failed
           input                         wdti         ;  //watchdog
           
           //seriart
           input                         rxd0_in      ; 
           output                        rxd0_out     ; 
           output                        txd0         ; 
                     
           input                         rxd1_in      ; 
           output                        rxd1_out     ; 
           output                        txd1         ; 
           
           //timernter input/output
           input                         t0           ; 
           input                         t1           ; 
           input                         t2           ; 
           input                         t2ex         ; 
           output                        t0_out       ; 
           output                        t1_out       ; 
           output                        t2_out       ; 
                     
           // interam interface
           output  [ 7:0]                iram_addr    ; 
           output  [ 7:0]                iram_data_out;  //mcu data to iram 
           input   [ 7:0]                iram_data_in ;  //iram data to mcu
           output                        iram_we_n    ;
           
           // interom interface
           output  [12:0] irom_addr    ; 
           input   [ 7:0]                irom_data    ; 
           output                        irom_rd_n    ;

	   //sfr inter
           output  [ 7:0]                sfr_addr     ; 
           output  [ 7:0]                sfr_data_out ; 
           input   [ 7:0]                sfr_data_in  ; 
           output                        sfr_wr       ; 
           output                        sfr_rd       ;
           
           //exterram interface or user-defined peripheral reg
           output  [12:0]                mem_addr     ; 
           output  [ 7:0]                mem_data_out ; 
           input   [ 7:0]                mem_data_in  ; 
           output                        mem_wr_n     ; 
           output                        mem_rd_n ;
		   
wire                       fire_out_first;
wire                       fire_out_second;
wire                       fire_cyc;
wire                       fire_cycsecond;
wire                       por_n        ; 
//(* dont_touch = "true" *)           wire                       rst_in_n     ; 
//(* dont_touch = "true" *)wire                       rst_out_n    ; 
 (* dont_touch = "true" *)wire                       test_mode_n  ; 
   
 (* dont_touch = "true" *)wire                       stop_mode_n  ; 
 (* dont_touch = "true" *)wire                       idle_mode_n  ;         

 (* dont_touch = "true" *)wire                       int0_n       ; 
 (* dont_touch = "true" *)wire                       int1_n       ; 
 (* dont_touch = "true" *)wire                       int2         ; 
 (* dont_touch = "true" *)wire                       int3_n       ; 
 (* dont_touch = "true" *)wire                       int4         ; 
 (* dont_touch = "true" *)wire                       int5_n       ; 

 (* dont_touch = "true" *)wire                       pfi          ;  //power failed
 (* dont_touch = "true" *)wire                       wdti         ;  //watchdog

 (* dont_touch = "true" *)wire                       rxd0_in      ; 
 (* dont_touch = "true" *)wire                       rxd0_out     ; 
 (* dont_touch = "true" *)wire                       txd0         ; 

 (* dont_touch = "true" *)wire                       rxd1_in      ; 
 (* dont_touch = "true" *)wire                       rxd1_out     ; 
 (* dont_touch = "true" *)wire                       txd1         ; 

 (* dont_touch = "true" *)wire                       t0           ; 
 (* dont_touch = "true" *)wire                       t1           ; 
 (* dont_touch = "true" *)wire                       t2           ; 
 (* dont_touch = "true" *)wire                       t2ex         ; 
 (* dont_touch = "true" *)wire                       t0_out       ; 
 (* dont_touch = "true" *)wire                       t1_out       ; 
 (* dont_touch = "true" *)wire                       t2_out       ; 

 (* dont_touch = "true" *)wire [ 7:0]                iram_addr    ; 
 (* dont_touch = "true" *)wire [ 7:0]                iram_data_out;  //mcu data to iram 
 (* dont_touch = "true" *)wire [ 7:0]                iram_data_in ;  //iram data to mcu
 (* dont_touch = "true" *)wire                       iram_we_n    ;

 (* dont_touch = "true" *)wire [12:0] irom_addr    ; 
 (* dont_touch = "true" *)wire [ 7:0]                irom_data    ; 
 (* dont_touch = "true" *)wire                       irom_rd_n    ;

 (* dont_touch = "true" *)wire [ 7:0]                sfr_addr     ; 
 (* dont_touch = "true" *)wire [ 7:0]                sfr_data_out ; 
 (* dont_touch = "true" *)wire [ 7:0]                sfr_data_in  ; 
 (* dont_touch = "true" *)wire                       sfr_wr       ; 
 (* dont_touch = "true" *)wire                       sfr_rd       ;

 (* dont_touch = "true" *)wire [12:0]                mem_addr     ; 
 (* dont_touch = "true" *)wire [ 7:0]                mem_data_out ; 
 (* dont_touch = "true" *)wire [ 7:0]                mem_data_in  ; 
 (* dont_touch = "true" *)wire                       mem_wr_n     ; 
 (* dont_touch = "true" *)wire                       mem_rd_n ;

 (* dont_touch = "true" *)wire [12:0] irom_addr_a;

  assign irom_addr=irom_addr_a[12:0];
  
(* keep_hierarchy = "yes" *)DW8051_core u_DW8051_core(.fire_out_first (fire_out_first) , 
                            .fire_out_second(fire_out_second),
                            .fire_cyc       (fire_cyc),
                            .fire_cycsecond (fire_cycsecond),
		                    .por_n          (por_n         ) ,  //mandatory for init , active at least 2 clk
//                            .rst_in_n       (rst_in_n      ) ,  //active at least 8 clk 
//                            .rst_out_n      (rst_out_n     ) ,
                            .test_mode_n    (test_mode_n   ) ,  //test mode for scan
                                                           
                            .stop_mode_n    (stop_mode_n   ) ,  //reset to exit stop mode
                            .idle_mode_n    (idle_mode_n   ) ,  //exit when reset or enabled interrupt occurs
                                                           
                            .sfr_addr       (sfr_addr      ) ,
                            .sfr_data_out   (sfr_data_out  ) ,
                            .sfr_data_in    (sfr_data_in   ) ,
                            .sfr_wr         (sfr_wr        ) ,
                            .sfr_rd         (sfr_rd        ) ,
                                                           
                            .mem_addr       (mem_addr      ) ,
                            .mem_data_out   (mem_data_out  ) ,
                            .mem_data_in    (mem_data_in   ) ,
                            .mem_wr_n       (mem_wr_n      ) ,
                            .mem_rd_n       (mem_rd_n      ) ,
                            .mem_pswr_n     (mem_pswr_n    ) ,
                            .mem_psrd_n     (mem_psrd_n    ) ,
                            .mem_ale        (              ) ,
                            .mem_ea_n       (1'b1          ) ,
                                                           
                            .int0_n         (int0_n        ) ,		// External Interrupt 0, std
                            .int1_n         (int1_n        ) ,		// External Interrupt 1, std
                            .int2           (int2          ) ,		// External Interrupt 2, ext
                            .int3_n         (int3_n        ) ,		// External Interrupt 3, ext
                            .int4           (int4          ) ,		// External Interrupt 4, ext
                            .int5_n         (int5_n        ) ,		// External Interrupt 5, ext
                                                           
                            .pfi            (pfi           ) ,		// power fail interrupt, ext
                            .wdti           (wdti          ) ,		// watchdog timer intr, ext
                                                           
                            .rxd0_in        (rxd0_in       ) ,		// serial port 0 input
                            .rxd0_out       (rxd0_out      ) ,		// serial port 0 output
                            .txd0           (txd0          ) ,		// serial port 0 output
                                                           
                            .rxd1_in        (rxd1_in       ) ,		// serial port 1 input
                            .rxd1_out       (rxd1_out      ) ,		// serial port 1 output
                            .txd1           (txd1          ) ,		// serial port 1 output
                                                           
                            .t0             (t0            ) ,		// Timer 0 external input
                            .t1             (t1            ) ,		// Timer 1 external input
                            .t2             (t2            ) ,		// Timer/Counter2 ext.input
                            .t2ex           (t2ex          ) ,		// Timer/Counter2 capt./reload
                            .t0_out         (t0_out        ) ,		// Timer/Counter0 ext. output
                            .t1_out         (t1_out        ) ,		// Timer/Counter1 ext. output
                            .t2_out         (t2_out        ) ,		// Timer/Counter2 ext. output
                                                           
                            .port_pin_reg_n (              ) ,
                            .p0_mem_reg_n   (              ) ,
                            .p0_addr_data_n (              ) ,
                            .p2_mem_reg_n   (              ) ,
                                                           
		            .iram_addr      (iram_addr     ) ,
		            .iram_data_out  (iram_data_in  ) ,
		            .iram_data_in   (iram_data_out ) ,
		            .iram_rd_n      (              ) ,
		            .iram_we1_n     (              ) ,
		            .iram_we2_n     (iram_we_n     ) ,
                                                           
		            .irom_addr      (irom_addr_a   ) ,
		            .irom_data_out  (irom_data     ) ,
		            .irom_rd_n      (irom_rd_n     ) ,
		            .irom_cs_n      (              )
		                                           );
endmodule

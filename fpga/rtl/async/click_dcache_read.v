`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////Asyncsys Lab////////////////////////////////////////
// Author: Zhenbang Kang
// Email: kangzhb20@lzu.edu.cn
// Create Date: 2022/06/10 
// Module Name: click_dcache_read
// Project Name: ac8051 (Asynchronous Click 8051)
// Target Devices: XC7A100TFGG484-2
// Tool Versions: Vivado 2019.1
// Description:Dcache Read ADL
// Revision: V2.0
//////////////////////////////////////////////////////////////////////////////////


module click_dcache_read(
                    in_R,
                    in_R_rst,
//                    rst_n,
                    in_A,
                    out_R,
                    fire_out
    );
    input   in_R;
    input   in_R_rst;
//    input   rst_n;
    output  in_A;
    output  out_R;
    output  fire_out;
					
(* dont_touch = "true" *)wire    in_R;
(* dont_touch = "true" *)reg    out_A;
wire    in_R_rst;
//(* dont_touch = "true" *) wire    rst_n;
(* dont_touch = "true" *)wire    in_A;
(* dont_touch = "true" *)wire    out_R;
wire    fire_out;
wire    fire_end;
wire    inv_fire_end;
(* dont_touch = "true" *)wire    out_R_1;
(* dont_touch = "true" *)wire    out_R_delay1;
(* dont_touch = "true" *)wire    in_A_delay2;
(* dont_touch = "true" *)wire    out_R_clickd1;
(* dont_touch = "true" *)wire    in_A_clickd1;
(* keep_hierarchy = "yes" *)click click_1(  .in_R(in_R),      .in_A(in_A),    .out_R(out_R_1),   .out_A(out_R),   .fire());
 /***************************************************delay_unit_1***************************************************/
(* keep_hierarchy = "yes" *)click click_delay1(  .in_R(out_R_1),   .in_A(),   .out_R(out_R_delay1),   .out_A(in_A_clickd1),   .fire());
(* keep_hierarchy = "yes" *)delay_unit delay_1(  .in_R(out_R_delay1),   .in_A(in_A_clickd1),  .out_R(out_R_clickd1),   .out_A(in_A_delay2),   .fire(fire_out));
(* keep_hierarchy = "yes" *)click click_delay2(  .in_R(out_R_clickd1),   .in_A(in_A_delay2),   .out_R(out_R),   .out_A(out_A),   .fire(fire_end));
assign inv_fire_end =~fire_end;
always@(posedge inv_fire_end or negedge in_R_rst)begin
    if(!in_R_rst)begin
        out_A <= 'b0;
    end else begin
        out_A   <= out_R; 
    end
end
endmodule

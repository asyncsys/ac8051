`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////Asyncsys Lab////////////////////////////////////////
// Author: Zhenbang Kang
// Email: kangzhb20@lzu.edu.cn
// Create Date: 2022/06/10 
// Module Name: click_control_second
// Project Name: ac8051 (Asynchronous Click 8051)
// Target Devices: XC7A100TFGG484-2
// Tool Versions: Vivado 2019.1
// Description:EX ADL
// Revision: V2.0
//////////////////////////////////////////////////////////////////////////////////

module click_control_second(
            rst_n,
            in_R,                   //  from        if
            in_R_rst,                              
//            in_A,                   //  to          if
            out_R_delay_final1,     //  to          dcache
            in_A_dcache_read,       //  from        dcache
//            in_A_click3,            //  to          dcache
            out_R_dcache_read,      //  from        dcache
            out_R_delay_final3,     //  to          dcache
            in_A_dcache_write,      //  from        dcache
            out_R_dcache_write,     //  from        dcache
            out_R_delay5,           //  to        ext_sram
            fire_ext_sram,
            fire_out,
            fire_uart,
            fire_tx_end,
            fire_end,
            fire_cycsecond,
            fire_rx,
            fire_rx_end
            
    );
	input     rst_n;
	input     in_R_rst;
    input     in_R;                   //  from        if                              
//    output    in_A;                   //  to          if
    output    out_R_delay_final1;     //  to          dcache
    input     in_A_dcache_read;       //  from        dcache
//    output    in_A_click3;            //  to          dcache
    input     out_R_dcache_read;      //  from        dcache
    output    out_R_delay_final3;     //  to          dcache
    input     in_A_dcache_write;      //  from        dcache
    input     out_R_dcache_write;     //  from        dcache
    output    out_R_delay5;           //  to        ext_sram
    output    fire_ext_sram;
    output    fire_out;
    output    fire_uart;
    output    fire_end;
    input     fire_tx_end;
    output    fire_cycsecond;
    output    fire_rx;
    input     fire_rx_end;
			
wire    rst_n;
wire    in_R_rst;
(* dont_touch = "true" *)wire    in_R;                   //  from        if                              
//(* dont_touch = "true" *)wire    in_A;                   //  to          if
(* dont_touch = "true" *)wire    out_R_delay_final1;     //  to          dcache
(* dont_touch = "true" *)wire    in_A_dcache_read;       //  from        dcache
//(* dont_touch = "true" *)wire    in_A_click3;            //  to          dcache
(* dont_touch = "true" *)wire    out_R_dcache_read;      //  from        dcache
(* dont_touch = "true" *)wire    out_R_delay_final3;     //  to          dcache
(* dont_touch = "true" *)wire    in_A_dcache_write;      //  from        dcache
(* dont_touch = "true" *)wire    out_R_dcache_write;     //  from        dcache
(* dont_touch = "true" *)wire    out_R_delay5;           //  to        ext_sram
wire    fire_ext_sram;
wire    fire_out;
wire    fire_uart;
wire    fire_uart_inv;
wire    fire_tx_end;
wire    fire_rx;
(* dont_touch = "true" *)wire    in_A1;
(* dont_touch = "true" *)wire    in_A2;
(* dont_touch = "true" *)wire    in_A4;
(* dont_touch = "true" *)wire    in_A_delay2;
(* dont_touch = "true" *)wire    in_A_delay4;
(* dont_touch = "true" *)wire    in_A_delay6;
(* dont_touch = "true" *)wire    in_A_delay8;
(* dont_touch = "true" *)wire    out_R1;
(* dont_touch = "true" *)wire    out_R2;
(* dont_touch = "true" *)wire    out_R3;
(* dont_touch = "true" *)wire    out_R4;
(* dont_touch = "true" *)wire    out_R_delay1;
(* dont_touch = "true" *)wire    out_R_delay3;
(* dont_touch = "true" *)wire    out_R_delay7;
(* dont_touch = "true" *)wire    out_R_delay_final2;
(* dont_touch = "true" *)wire    out_R_delay_final4;
(* dont_touch = "true" *)reg    out_A_delay_final4;
(* dont_touch = "true" *)wire    out_A1;
(* dont_touch = "true" *)wire    out_A2;
(* dont_touch = "true" *)wire    out_A3;
(* dont_touch = "true" *)wire    out_A4;
(* dont_touch = "true" *)wire    out_A_delay1;
(* dont_touch = "true" *)wire    out_A_delay2;
wire    fire1;
wire    fire2;
wire    fire3;
wire    fire4;

wire    fire_cyc2;
wire    fire_cyc3;
wire    fire_cyc4;
wire    fire_cycsecond;
wire    fire_end;
wire    fire_end_inv;

(* dont_touch = "true" *)reg    in_R_end;
(* dont_touch = "true" *)wire    out_R_end1;
(* dont_touch = "true" *)wire    out_R_end2;
(* dont_touch = "true" *)wire    in_A_end1;
(* dont_touch = "true" *)wire    in_A_end2;
(* dont_touch = "true" *)wire    in_A_end3;    
(* dont_touch = "true" *)wire    out_R_final;
(* dont_touch = "true" *)reg    out_A_final;

(* dont_touch = "true" *)wire    out_R_clickd1;
(* dont_touch = "true" *)wire    out_R_clickd2;
(* dont_touch = "true" *)wire    out_R_clickd3;
(* dont_touch = "true" *)wire    out_R_clickd4;
(* dont_touch = "true" *)wire    in_A_clickd1;
(* dont_touch = "true" *)wire    in_A_clickd2;
(* dont_touch = "true" *)wire    in_A_clickd3;
(* dont_touch = "true" *)wire    in_A_clickd4;
 wire   fire_rx_end; 
 reg    in_R3;
 wire   inv_fire_rx;
 reg    out_A_delay4;  
(* keep_hierarchy = "yes" *)click click_1(  .in_R(in_R),      .in_A(),    .out_R(out_R1),   .out_A(out_R_delay_final1),   .fire(fire1));
/***************************************************delay_unit_1***************************************************/
(* keep_hierarchy = "yes" *)click click_delay1(  .in_R(out_R1),   .in_A(),   .out_R(out_R_delay1),   .out_A(in_A_clickd1),   .fire());
(* keep_hierarchy = "yes" *)delay_unit delay_1(  .in_R(out_R_delay1),   .in_A(in_A_clickd1),  .out_R(out_R_clickd1),   .out_A(in_A_delay2),   .fire(fire_ext_sram));
(* keep_hierarchy = "yes" *)click click_delay2(  .in_R(out_R_clickd1),   .in_A(in_A_delay2),   .out_R(out_R_delay_final1),   .out_A(in_A_dcache_read),   .fire());
/****************************************************************************************************************/   
(* keep_hierarchy = "yes" *)click click_2(  .in_R(out_R_delay_final1),    .in_A(),    .out_R(out_R2),   .out_A(out_R_delay_final2),   .fire(fire2));
/***************************************************delay_unit_2***************************************************/
(* keep_hierarchy = "yes" *)click click_delay3(  .in_R(out_R2),   .in_A(),   .out_R(out_R_delay3),   .out_A(in_A_clickd2),   .fire());
(* keep_hierarchy = "yes" *)delay_unit delay_2(  .in_R(out_R_delay3),   .in_A(in_A_clickd2),  .out_R(out_R_clickd2),   .out_A(in_A_delay4),   .fire(fire_cyc2));
(* keep_hierarchy = "yes" *)click click_delay4(  .in_R(out_R_clickd2),   .in_A(in_A_delay4),   .out_R(out_R_delay_final2),   .out_A(out_A_delay4),   .fire(fire_rx));
/***************************************************rx**************************************************************/
assign inv_fire_rx = ~fire_rx;
always@(posedge inv_fire_rx or negedge in_R_rst)begin
    if(!in_R_rst)begin
        out_A_delay4 <= 'b0;
    end else begin
        out_A_delay4 <= out_R_delay_final2;
    end
end 

always@(posedge fire_rx_end or negedge rst_n)begin
    if(!rst_n)begin
        in_R3 <= 'b0;
    end else begin
        in_R3 <= ~in_R3;
    end
end



/****************************************************************************************************************/
(* keep_hierarchy = "yes" *)click click_3(  .in_R(in_R3),    .in_A(),    .out_R(out_R3),   .out_A(out_R_delay_final3),   .fire(fire3));
/***************************************************delay_unit_3***************************************************/
(* keep_hierarchy = "yes" *)click click_delay5(  .in_R(out_R3),   .in_A(),   .out_R(out_R_delay5),   .out_A(in_A_clickd3),   .fire());
(* keep_hierarchy = "yes" *)delay_unit delay_3(  .in_R(out_R_delay5),   .in_A(in_A_clickd3),  .out_R(out_R_clickd3),   .out_A(in_A_delay6),   .fire(fire_cyc3));
(* keep_hierarchy = "yes" *)click click_delay6(  .in_R(out_R_clickd3),   .in_A(in_A_delay6),   .out_R(out_R_delay_final3),   .out_A(in_A_dcache_write),   .fire());
/****************************************************************************************************************/
(* keep_hierarchy = "yes" *)click click_4(  .in_R(out_R_delay_final3),    .in_A(),    .out_R(out_R4),   .out_A(out_R_delay_final4),   .fire(fire4));
/***************************************************delay_unit_4***************************************************/
(* keep_hierarchy = "yes" *)click click_delay7(  .in_R(out_R4),   .in_A(),   .out_R(out_R_delay7),   .out_A(in_A_clickd4),   .fire());
(* keep_hierarchy = "yes" *)delay_unit delay_4(  .in_R(out_R_delay7),   .in_A(in_A_clickd4),  .out_R(out_R_clickd4),   .out_A(in_A_delay8),   .fire(fire_cyc4));
(* keep_hierarchy = "yes" *)click click_delay8(  .in_R(out_R_clickd4),   .in_A(in_A_delay8),   .out_R(out_R_delay_final4),   .out_A(out_A_delay_final4),   .fire(fire_uart));
/****************************************************************************************************************/
assign fire_uart_inv = ~fire_uart;
always@(posedge fire_uart_inv or negedge in_R_rst)begin
    if(!in_R_rst)begin
        out_A_delay_final4 <= 'b0;
    end else begin
        out_A_delay_final4 <= out_R_delay_final4;  
    end
end
always@(posedge fire_tx_end or negedge rst_n)begin
    if(!rst_n)begin
        in_R_end <= 'b0;
    end else begin
        in_R_end <= out_R_delay_final4;
    end
end
(* keep_hierarchy = "yes" *)click click_end1(.in_R(in_R_end),.in_A(in_A_end1),.out_R(out_R_end1),.out_A(in_A_end2),.fire());    
(* keep_hierarchy = "yes" *)click click_end2(.in_R(out_R_end1),.in_A(in_A_end2),.out_R(out_R_end2),.out_A(in_A_end3),.fire());
(* keep_hierarchy = "yes" *)click click_end3(.in_R(out_R_end2),.in_A(in_A_end3),.out_R(out_R_final),.out_A(out_A_final),.fire(fire_end)); 
assign fire_end_inv = ~fire_end;
always@(posedge fire_end_inv or negedge in_R_rst)begin
    if(!in_R_rst)begin
        out_A_final <= 'b0;
    end else begin
        out_A_final <= out_R_final;
    end 
end   
assign fire_cycsecond = fire_ext_sram | fire_cyc2 | fire_cyc3 | fire_cyc4;
assign fire_out=fire1|fire2|fire3|fire4;
endmodule

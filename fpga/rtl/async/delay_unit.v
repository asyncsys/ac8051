`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////Asyncsys Lab////////////////////////////////////////
// Author: Zhenbang Kang
// Email: kangzhb20@lzu.edu.cn
// Create Date: 2022/06/10 
// Module Name: delay_unit
// Project Name: ac8051 (Asynchronous Click 8051)
// Target Devices: XC7A100TFGG484-2
// Tool Versions: Vivado 2019.1
// Description:Delay Extension
// Revision: V2.0
//////////////////////////////////////////////////////////////////////////////////


module delay_unit(
            in_R,
            in_A,
            out_R,
            out_A,
            fire
    );
    input   in_R;
    input   out_A;
    output  in_A;
    output  out_R;
    output  fire;
    
    wire    in_R;
    wire    in_A;
    wire    out_R;
    wire    out_A;
    wire    fire;
    
//    wire    in_A_delay1;
    wire    in_A_delay2;
    wire    in_A_delay3;
    wire    in_A_delay4;
    wire    in_A_delay5;
    wire    in_A_delay6;
    wire    in_A_delay7;
    wire    in_A_delay8;
    wire    in_A_delay9;
    wire    in_A_delay10;
    wire    in_A_delay11;
    wire    in_A_delay12;
    wire    in_A_delay13;
    wire    in_A_delay14;   
    
    wire    out_R_delay1;
    wire    out_R_delay2;
    wire    out_R_delay3;
    wire    out_R_delay4;
    wire    out_R_delay5;
    wire    out_R_delay6;
    wire    out_R_delay7;
    wire    out_R_delay8;
    wire    out_R_delay9;
    wire    out_R_delay10;
    wire    out_R_delay11;
    wire    out_R_delay12;
    wire    out_R_delay13;
(* keep_hierarchy = "yes" *)click click_delay1
                                    (  .in_R    (in_R),   
                                       .in_A    (in_A),   
                                       .out_R   (out_R_delay1),   
                                       .out_A   (in_A_delay2),   
                                       .fire    ()
                                     );
(* keep_hierarchy = "yes" *)click click_delay2         
                                    (  .in_R    (out_R_delay1),
                                       .in_A    (in_A_delay2),    
                                       .out_R   (out_R_delay2),    
                                       .out_A   (in_A_delay3),    
                                       .fire    ()     
                                     );
(* keep_hierarchy = "yes" *)click click_delay3         
                                    (  .in_R    (out_R_delay2),
                                       .in_A    (in_A_delay3),    
                                       .out_R   (out_R_delay3),    
                                       .out_A   (in_A_delay4),    
                                       .fire    ()     
                                     );
(* keep_hierarchy = "yes" *)click click_delay4         
                                    (  .in_R    (out_R_delay3),
                                       .in_A    (in_A_delay4),    
                                       .out_R   (out_R_delay4),    
                                       .out_A   (in_A_delay5),    
                                       .fire    ()     
                                     );
(* keep_hierarchy = "yes" *)click click_delay5         
                                    (  .in_R    (out_R_delay4),
                                       .in_A    (in_A_delay5),    
                                       .out_R   (out_R_delay5),    
                                       .out_A   (in_A_delay6),    
                                       .fire    ()     
                                     );
(* keep_hierarchy = "yes" *)click click_delay6         
                                    (  .in_R    (out_R_delay5),
                                       .in_A    (in_A_delay6),    
                                       .out_R   (out_R_delay6),    
                                       .out_A   (in_A_delay7),    
                                       .fire    ()     
                                     ); 
(* keep_hierarchy = "yes" *)click click_delay7         
                                    (  .in_R    (out_R_delay6),
                                       .in_A    (in_A_delay7),    
                                       .out_R   (out_R_delay7),    
                                       .out_A   (in_A_delay8),    
                                       .fire    (fire)     
                                     ); 
(* keep_hierarchy = "yes" *)click click_delay8         
                                    (  .in_R    (out_R_delay7),
                                       .in_A    (in_A_delay8),    
                                       .out_R   (out_R_delay8),    
                                       .out_A   (in_A_delay9),    
                                       .fire    ()     
                                     ); 
(* keep_hierarchy = "yes" *)click click_delay9         
                                    (  .in_R    (out_R_delay8),
                                       .in_A    (in_A_delay9),    
                                       .out_R   (out_R_delay9),    
                                       .out_A   (in_A_delay10),    
                                       .fire    ()     
                                     ); 
(* keep_hierarchy = "yes" *)click click_delay10         
                                    (  .in_R    (out_R_delay9),
                                       .in_A    (in_A_delay10),    
                                       .out_R   (out_R_delay10),    
                                       .out_A   (in_A_delay11),    
                                       .fire    ()     
                                     ); 
(* keep_hierarchy = "yes" *)click click_delay11         
                                    (  .in_R    (out_R_delay10),
                                       .in_A    (in_A_delay11),    
                                       .out_R   (out_R_delay11),    
                                       .out_A   (in_A_delay12),    
                                       .fire    ()     
                                     ); 
(* keep_hierarchy = "yes" *)click click_delay12         
                                    (  .in_R    (out_R_delay11),
                                       .in_A    (in_A_delay12),    
                                       .out_R   (out_R_delay12),    
                                       .out_A   (in_A_delay13),    
                                       .fire    ()     
                                     ); 
(* keep_hierarchy = "yes" *)click click_delay13         
                                    (  .in_R    (out_R_delay12),
                                       .in_A    (in_A_delay13),    
                                       .out_R   (out_R_delay13),    
                                       .out_A   (in_A_delay14),    
                                       .fire    ()     
                                     ); 
(* keep_hierarchy = "yes" *)click click_delay14         
                                    (  .in_R    (out_R_delay13),
                                       .in_A    (in_A_delay14),    
                                       .out_R   (out_R),    
                                       .out_A   (out_A),    
                                       .fire    ()     
                                     );                                                                                                                                                                                                                                                                                                                                                                                                                                  
endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////Asyncsys Lab////////////////////////////////////////
// Author: Zhenbang Kang
// Email: kangzhb20@lzu.edu.cn
// Create Date: 2022/06/10 
// Module Name: click_start
// Project Name: ac8051 (Asynchronous Click 8051)
// Target Devices: XC7A100TFGG484-2
// Tool Versions: Vivado 2019.1
// Description: Start ADL
// Revision: V2.0
//////////////////////////////////////////////////////////////////////////////////

module click_start(
                    in_R,
                    out_A,
                    out_R,                    
                    fire_out
    );
input   in_R;
input   out_A;
output  out_R;                    
output  fire_out;

(* dont_touch = "true" *)wire    in_R;
(* dont_touch = "true" *)wire    out_A;
(* dont_touch = "true" *)wire    out_R;                    
wire    fire_out;   
(* dont_touch = "true" *)wire    out_R_1;
(* dont_touch = "true" *)wire    out_R_delay1;
(* dont_touch = "true" *)wire    in_A_delay1;
(* dont_touch = "true" *)wire    in_A_delay2;
(* keep_hierarchy = "yes" *)click click_1(  .in_R(in_R),      .in_A(),    .out_R(out_R_1),   .out_A(in_A_delay1),   .fire());
/***************************************************delay_unit_1***************************************************/
(* keep_hierarchy = "yes" *)click click_delay1(  .in_R(out_R_1),   .in_A(in_A_delay1),   .out_R(out_R_delay1),   .out_A(in_A_delay2),   .fire());
(* keep_hierarchy = "yes" *)click click_delay2(  .in_R(out_R_delay1),   .in_A(in_A_delay2),   .out_R(out_R),   .out_A(out_A),   .fire(fire_out));
endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////Asyncsys Lab////////////////////////////////////////
// Author: Zhenbang Kang
// Email: kangzhb20@lzu.edu.cn
// Create Date: 2022/06/10 
// Module Name: icache_control
// Project Name: ac8051 (Asynchronous Click 8051)
// Target Devices: XC7A100TFGG484-2
// Tool Versions: Vivado 2019.1
// Description:Icache Module
// Revision: V2.0
//////////////////////////////////////////////////////////////////////////////////

module icache_control(                      
					 in_R,   //out_R_delay_final2
			         in_A,   //in_A_icache
			         second_begin,
//                     rst_n,
                     rom_addr,
                     rom_rd,
                     out_R_icache,
                     in_A_icache_click2,
					 icache_data 
    ); 
input          in_R;   //out_R_delay_final2
input          in_A;   //in_A_icache
input          second_begin;
//input          rst_n;
input  [12:0]  rom_addr;
input          rom_rd;
output         out_R_icache;
output         in_A_icache_click2;
output [7:0]   icache_data;

(* dont_touch = "true" *)wire           in_R;   //out_R_delay_final2
(* dont_touch = "true" *)wire           in_A;   //in_A_icache
//(* dont_touch = "true" *)wire           rst_n;
(* dont_touch = "true" *)wire   [12:0]   rom_addr;
(* dont_touch = "true" *)wire           rom_rd;
(* dont_touch = "true" *)wire           out_R_icache;
(* dont_touch = "true" *)wire           in_A_icache_click2;
(* dont_touch = "true" *)wire   [7:0]   rom_data;
wire                   fire_out; 
(* dont_touch = "true" *)wire                   icache_ena;
(* dont_touch = "true" *)wire     [7:0]         rom_tmp_data;
   wire         icache_wea;
   wire   [12:0] icache_addr;
   wire   [ 7:0] icache_data;
wire         second_begin;  
(* keep_hierarchy = "yes" *)click_interact   interact1(
                    .in_R           (           in_R          ),
                    .out_A          (           in_A          ),
//                    .rst_n          (        rst_n            ),
                    .in_A           (   in_A_icache_click2    ),
                    .out_R          (       out_R_icache      ),
                    .fire_out       (       fire_out          )
    );

   
   assign icache_ena =  (second_begin==1'b0)? 0 : rom_rd; 
   assign icache_wea =  (second_begin==1'b0)? 0 : ~rom_rd; 
   assign icache_addr = (second_begin==1'b0)? 12'd0 : rom_addr;
   assign icache_data = (second_begin==1'b0)? 8'd0  : rom_data;
(* keep_hierarchy = "yes" *)blk_mem_gen_0 icache(
                .clka(fire_out),
                .ena(icache_ena),
                .wea(icache_wea),
                .addra(icache_addr),
                .dina(rom_tmp_data),
                .douta(rom_data)
                ); 
//     sram #( .data_width(8), .data_depth(256), .addr_width(8))
//             u_sram(.fire(work),.data(ext_data),.addr(ADDR),.cen(CEN),.wen(WEN),.Q(rom_data));
endmodule

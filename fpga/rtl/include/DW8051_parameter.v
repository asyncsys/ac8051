//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////Asyncsys Lab////////////////////////////////////////
// Author: Zhenbang Kang
// Email: kangzhb20@lzu.edu.cn
// Create Date: 2022/06/10 
// Module Name: chip_top
// Project Name: ac8051 (Asynchronous Click 8051)
// Target Devices: XC7A100TFGG484-2
// Tool Versions: Vivado 2019.1
// Description:Include Information 
// Revision: V2.0
//////////////////////////////////////////////////////////////////////////////////


// reuse-pragma beginAttr Description
// The amount of Internal (scratchpad) RAM to be configured.
// Valid sizes are: 128 bytes and 256 bytes.
// reuse-pragma endAttr
// reuse-pragma attr EnumValues 0 1
// reuse-pragma attr SymbolicNames {128 bytes} {256 bytes}
// reuse-pragma attr Label Size of Internal RAM
// reuse-pragma startSub [ReplaceConstantParam %subText]
`define ram_256 1

// reuse-pragma attr Description Should Timer 2 be included in the core ?
// reuse-pragma attr EnumValues 0 1
// reuse-pragma attr Label Timer 2
// reuse-pragma attr SymbolicNames {Removed} {Included}
// reuse-pragma startSub [ReplaceConstantParam %subText]
`define timer2 0

// reuse-pragma beginAttr Description
// The width of the Internal ROM address bus. Valid values are from 0 to 16 bits.
// Selecting 0 means no Internal ROM is present.
// reuse-pragma endAttr
// reuse-pragma attr MinValue 0
// reuse-pragma attr MaxValue 16
// reuse-pragma attr Label Internal ROM Address Bus Width (bits)
// reuse-pragma startSub [ReplaceConstantParam %subText]
`define rom_addr_size 13

// reuse-pragma beginAttr Description
// The number of serial ports required in the core.
// Valid values are: None, One and Two.
// reuse-pragma endAttr
// reuse-pragma attr EnumValues 0 1 2
// reuse-pragma attr Label Number of Serial Ports
// reuse-pragma attr SymbolicNames None One Two
// reuse-pragma startSub [ReplaceConstantParam %subText]
`define serial 1

// reuse-pragma beginAttr Description
// The Interrupt Unit used in the core.
// Valid values are: Standard and Extended.
// reuse-pragma endAttr
// reuse-pragma attr EnumValues 0 1
// reuse-pragma attr Label Interrupt Unit Type
// reuse-pragma attr SymbolicNames {Standard} {Extended}
// reuse-pragma startSub [ReplaceConstantParam %subText]
`define extd_intr 0

`define ROM_DATA_WIDTH 8
`define ROM_DATA_DEPTH 256
`define ROM_ADDR_WIDTH 8


`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////Asyncsys Lab////////////////////////////////////////
// Author: Zhenbang Kang
// Email: kangzhb20@lzu.edu.cn
// Create Date: 2022/06/10 
// Module Name: tx_final
// Project Name: ac8051 (Asynchronous Click 8051)
// Target Devices: XC7A100TFGG484-2
// Tool Versions: Vivado 2019.1
// Description:TX control delay
// Revision: V2.0
//////////////////////////////////////////////////////////////////////////////////


module tx_final(        in_R,      
                        in_A,    
                        out_R,   
                        out_A,   
                        fire  
                );
    
    input   in_R;
    input   out_A;
    output  out_R;
    output  in_A;
    output  fire;
    
 (* dont_touch = "true" *)wire    in_R;
 (* dont_touch = "true" *)wire    out_R;
 (* dont_touch = "true" *)wire    out_A;
 (* dont_touch = "true" *)wire    in_A;
                          wire    fire;
 (* dont_touch = "true" *)wire    out_R_tx_end;
 (* dont_touch = "true" *)wire    in_A_tx_end;
 
(* keep_hierarchy = "yes" *)click tx_final(  
                                    .in_R(in_R),   
                                    .in_A(),  
                                    .out_R(out_R_tx_end),   
                                    .out_A(in_A_tx_end),   
                                    .fire(fire));
(* keep_hierarchy = "yes" *)click tx_end(  
                                    .in_R(out_R_tx_end),   
                                    .in_A(in_A_tx_end),  
                                    .out_R(out_R),   
                                    .out_A(out_A),   
                                    .fire());    
endmodule
